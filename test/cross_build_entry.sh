#!/bin/bash
#
# Entry point script for cross-platform builds.
#
# This script reads the emulator from CMake configuration and hands it to the test script.
#
# Copyright 2022 Max Harmathy <maxh@if.de>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

set -e
shopt -s nullglob

HERE="$(dirname "$(realpath "$0")")"
BUILD_DIR="$PWD/build"

cmake -B "$BUILD_DIR" -G Ninja "$PWD"
ninja -C "$BUILD_DIR"
if [ -n "$CMAKE_TOOLCHAIN_FILE" ]
then
  EMULATOR=$(grep -oP '(?<=set\(CMAKE_CROSSCOMPILING_EMULATOR )/usr/bin/qemu[^)]*(?=\))' "$CMAKE_TOOLCHAIN_FILE")
fi
if [ -f "$EMULATOR" ]
then
  echo "Using $EMULATOR as emulator."
  "$HERE/ascii_art.sh" -e "$EMULATOR" "$BUILD_DIR/drunken_bishop"
else
  "$HERE/ascii_art.sh" "$BUILD_DIR/drunken_bishop"
fi

