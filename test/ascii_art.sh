#!/bin/bash
#
# Copyright 2022 Max Harmathy <maxh@if.de>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

set -e
shopt -s nullglob

DESCRIPTION="This script is for testing the ascii-art output of stdout against a reference."

THIS="$(basename "$0")"
HERE="$(dirname "$(realpath "$0")")"

function usage_print_optional_parameter {
  local short="$1"
  local long="$2"
  local variable="$3"
  local description="$4"
  printf " -%s | --%s %s\t%s\n" "$short" "$long" "$variable" "$description"
}

function usage_print_option_switch {
  local short="$1"
  local long="$2"
  local description="$3"
  printf " -%s | --%s\t%s\n" "$short" "$long" "$description"
}

function usage {
  printf "%s [OPTIONS] EXECUTABLE\n" "$THIS"
  printf "%s\n" "$DESCRIPTION"
  printf "\nOptions:\n"
  usage_print_option_switch "h" "help" "display this help message"
  usage_print_optional_parameter "i" "input" "DIR" "directory of reference files"
  usage_print_optional_parameter "e" "executor" "EXECUTOR" "executor, i.e. cross-platform emulator, for the binary."
}

function process_args {
  local count=0
  while (("$#"))
  do
    case "$1" in
    "-h"|"--help")
      usage
      exit 0
    ;;
    "-i"|"--input")
      if [ -z "$2" ]
      then
        usage
        exit 1
      fi
      INPUT="$2"
      shift
    ;;
    "-e"|"--executor")
        if [ -z "$2" ]
        then
          usage
          exit 1
        fi
        EXECUTOR="$2"
        shift
      ;;
    *)
      case $count in
      0)
        EXECUTABLE="$1"
        ;;
      *)
        usage
        exit 1
        ;;
      esac
      ((count = count + 1))
      ;;
    esac
    shift
  done
}

function report_error {
  local message="$1"
  echo "$message"
  usage
  return 1
}

INPUT="$HERE/data"
process_args "$@"
if [ -z "$EXECUTABLE" ]
then
  report_error "No EXECUTABLE given"
fi
if ! [ -f "$EXECUTABLE" ]
then
  report_error "EXECUTABLE does not exist"
fi
if [ -z "$INPUT" ]
then
  report_error "no input directory given"
fi
if ! [ -d "$INPUT" ]
then
  report_error "could not find input directory"
fi

test_count='0'
failed_count='0'

for file in "$INPUT"/*.in
do
  outfile="${file%.in}.out"
  if [ -f "$outfile" ]
  then
    expected=$(<"$outfile")
  fi
  ((++test_count))

  if [ -n  "$EXECUTOR" ]
  then
    # we actually want the parameters to expand here
    # shellcheck disable=SC2046
    actual=$("$EXECUTOR" "$EXECUTABLE" $(<"$file"))
  else
    # shellcheck disable=SC2046
    actual=$("$EXECUTABLE" $(<"$file"))
  fi

  echo -n  "$(basename "${file%.in}"): "
  if ! [ "$expected" = "$actual" ]
  then
    echo "failed"
    ((++failed_count))
    printf "INPUT: %s\n\nEXPECTED:\n%s\nGOT:\n%s\n" "$file" "$expected" "$actual"
  else
    echo "passed"
  fi
done

if (( failed_count == 0 ))
then
  printf "All %d tests passed\n" "$test_count"
else
  printf "Failed %d out of %d tests" "$failed_count" "$test_count"
  exit 1
fi

