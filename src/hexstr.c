/* hexstr.c
 *
 * Copyright 2022 Max Harmathy <maxh@if.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "hexstr.h"
#include <stdio.h>
#include <string.h>

drubis_bin_data drubis_input_from_string(const char* string) {
    drubis_bin_data result;
    result.length = strlen(string);
    result.data = malloc(sizeof(u_int8_t) * result.length);
    for (size_t i = 0; i < result.length; ++i) {
        result.data[i] = (u_int8_t) string[i];
    }
    return result;
}

drubis_bin_data drubis_hex_string_to_data(const char* string) {

    drubis_bin_data result = {.length = 0, .data = NULL};

    if (string == NULL) {
        return result;
    }

    size_t length = strlen(string);
    if (length == 0) {
        return result;
    }

    char *working_copy, *insert_point;
    unsigned int bytes_length = length / 2;

    if (length % 2 == 1) {
        working_copy = malloc(sizeof(char) * (length + 1));
        working_copy[0] = '0';
        insert_point = working_copy + 1;
        ++bytes_length;
    } else {
        working_copy = malloc(sizeof(char) * length);
        insert_point = working_copy;
    }
    strncpy(insert_point, string, length);

    u_int8_t* result_data = malloc(sizeof(u_int8_t) * (bytes_length));

    char* position = working_copy;
    for (int i = 0; i < bytes_length; ++i) {
        sscanf(position, "%2hhx", &result_data[i]);

        // check if a zero value derives from the input data or indicates an
        // invalid input
        if (result_data[i] == 0 && !(position[0] == '0' && position[1] == '0')) {
            free(result_data);
            free(working_copy);
            return result;
        }
        position += 2;
    }
    free(working_copy);
    result.length = bytes_length;
    result.data = result_data;
    return result;
}