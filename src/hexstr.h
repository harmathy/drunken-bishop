/* hexstr.h
 *
 * Copyright 2022 Max Harmathy <maxh@if.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DRUNKEN_BISHOP_HEXSTR_H
#define DRUNKEN_BISHOP_HEXSTR_H

#include <stdlib.h>

// simple wrapper for binary data array with length
struct drubis_bin_data_s {
    size_t length;
    u_int8_t* data;
};

// simple wrapper for binary data array with length
typedef struct drubis_bin_data_s drubis_bin_data;

// convert the characters of the string as is into binary array
drubis_bin_data drubis_input_from_string(const char* string);

// Interpret the string as hexadecimal data and create a binary array.
// In case of invalid input, i.e. NULL, empty string or invalid characters,
// the data attribute will be NULL and length 0.
drubis_bin_data drubis_hex_string_to_data(const char* string);

#endif // DRUNKEN_BISHOP_HEXSTR_H
