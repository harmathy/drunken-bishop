/* main.c
 *
 * Copyright 2022 Max Harmathy <maxh@if.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "drunken_bishop.h"
#include "hexstr.h"
#include <argp.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#define HEIGHT_KEY 'H'
#define WIDTH_KEY 'W'
#define HEX_DATA_KEY 'x'

#define PARAMETER_NUMBERS_BASE 10

#define PARAMETER_ERROR 1

// Wrap the parameters and add state data for conveying data to one call of the
// parsing function to another.
struct parsing_state {
    struct drubis_parameters_s* parameters;
    char* data_string;
    int argument_count;
    bool hex_stream;
};

// parse function used in combination with argp
static error_t parse_parameters(int key, char* arg, struct argp_state* state) {

    struct parsing_state* parsing_state = state->input;

    struct drubis_parameters_s* parameters = parsing_state->parameters;

    switch (key) {
    case HEIGHT_KEY: {
        long height = strtol(arg, NULL, PARAMETER_NUMBERS_BASE);
        if (height > DRUBIS_MAXIMUM_HEIGHT) {
            fprintf(
                stderr, "%ld exceeds the maximum height of %d\n", height,
                DRUBIS_MAXIMUM_HEIGHT
            );
            return PARAMETER_ERROR;
        }
        parameters->height = (int) height;
        break;
    }
    case WIDTH_KEY: {
        long width = strtol(arg, NULL, PARAMETER_NUMBERS_BASE);
        if (width > DRUBIS_MAXIMUM_WIDTH) {
            fprintf(
                stderr, "%ld exceeds the maximum width of %d\n", width,
                DRUBIS_MAXIMUM_WIDTH
            );
            return PARAMETER_ERROR;
        }
        parameters->width = (int) width;
        break;
    }
    case HEX_DATA_KEY: {
        parsing_state->hex_stream = true;
        break;
    }
    case ARGP_KEY_ARG: {
        parsing_state->data_string = arg;
        ++(parsing_state->argument_count);
        break;
    }
    case ARGP_KEY_END: {
        if (parsing_state->argument_count > 1) {
            argp_failure(state, 1, 0, "too many arguments");
        } else if (parsing_state->argument_count < 1) {
            argp_failure(state, 1, 0, "missing argument");
        }
        break;
    }
    default:
        return ARGP_ERR_UNKNOWN;
    }
    return 0;
}

int main(int argc, char** argv) {

    struct argp_option options[] = {
        {"height", HEIGHT_KEY, "HEIGHT", 0, "height of the play field"},
        {"width", WIDTH_KEY, "WIDTH", 0, "width of the play field"},
        {"hex", HEX_DATA_KEY, NULL, 0, "interpret DATA as hex stream"},
        {"DATA", 0, "DATA", OPTION_DOC, "data string as data_string"},
        {0}

    };

    struct drubis_parameters_s parameters;
    struct parsing_state parse_data;

    drubis_init_parameters(&parameters);

    // initialize parsing state
    parse_data.parameters = &parameters;
    parse_data.argument_count = 0;
    parse_data.data_string = NULL;
    parse_data.hex_stream = false;

    // parse commandline argument_count
    struct argp parser = {options, parse_parameters, "DATA", 0};
    error_t status = argp_parse(&parser, argc, argv, 0, 0, &parse_data);

    if (status != 0) {
        return status;
    }

    drubis_bin_data converted_input;
    if (parse_data.hex_stream) {
        converted_input = drubis_hex_string_to_data(parse_data.data_string);
        if (converted_input.length == 0) {
            fprintf(stderr, "failed to read DATA in hex format.\n");
            return 1;
        }
    } else {
        converted_input = drubis_input_from_string(parse_data.data_string);
    }
    parameters.input = converted_input;
    drubis_play_field field = drubis_let_him_go(parameters);
    char* ascii_art = drubis_play_field_to_ascii_art(field);

    printf("%s", ascii_art);

    drubis_destroy_play_field(&field);
    return status;
}
