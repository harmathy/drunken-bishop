/* main.h
 *
 * Copyright 2022 Max Harmathy <maxh@if.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DRUNKEN_BISHOP_DRUNKEN_BISHOP_H
#define DRUNKEN_BISHOP_DRUNKEN_BISHOP_H

#include "hexstr.h"

#include <stdio.h>
#include <stdlib.h>

#define DRUBIS_MAXIMUM_WIDTH 64
#define DRUBIS_MAXIMUM_HEIGHT 64

#define DRUBIS_DEFAULT_HEIGHT 9
#define DRUBIS_DEFAULT_WIDTH 17

#define BORDER_CORNER '+'
#define BORDER_HORIZONTAL '-'
#define BORDER_VERTICAL '|'

struct drubis_parameters_s {
    unsigned int width, height;
    drubis_bin_data input;
};

// parameters for the drunken bishop
typedef struct drubis_parameters_s drubis_parameters;

// initialize the parameters
void drubis_init_parameters(drubis_parameters* parameters);

struct drubis_play_field_s {
    unsigned int width, height;
    unsigned int* squares;
};

// play field for the drunken bishop
typedef struct drubis_play_field_s drubis_play_field;

// initialize the play field
void drubis_init_play_field(
    drubis_play_field* play_field, unsigned int width, unsigned int height
);

// free the memory of the play field data
void drubis_destroy_play_field(drubis_play_field* play_field);

// the figures to be set on the play field, depending on how often the bishop
// visited a certain square.
static char drubis_figures[] = {
    ' ', '.', 'o', '+', '=', '*', 'B', '0',
    'X', '@', '%', '&', '#', '/', '^'

};

// Simulate the drunken bishop wandering on the play field as determined by the
// parameters. The caller is responsible for calling drubis_destroy_play_field on the
// returned object.
drubis_play_field drubis_let_him_go(drubis_parameters parameters);

// Generate an ascii art representation of the play field (similar to openssh).
// The caller is responsible for cleaning the resulting string.
char* drubis_play_field_to_ascii_art(drubis_play_field field);

#endif // DRUNKEN_BISHOP_DRUNKEN_BISHOP_H
